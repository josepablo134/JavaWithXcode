JCC = javac

JFLAGS = -g

default: $(subst .java,.class,$(wildcard *.java))

%.class : %.java
	$(JCC)	$(JFLAGS)	$<

clean:
	$(RM) *.class
